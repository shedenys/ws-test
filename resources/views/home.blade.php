@extends('layout')

@section('main')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Должности</h5>
                            <a href="/positions" class="btn btn-primary">Перейти</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Работники</h5>
                            <a href="/workers" class="btn btn-primary">Перейти</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection