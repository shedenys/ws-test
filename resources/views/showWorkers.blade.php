@extends('layout')

@section('main')

    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    @include('partials.messages')
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="clearfix">
                        <div class="float-left">
                            <a href="/workers/add" class="btn btn-primary">Добавить работника</a>
                        </div>
                        <div class="float-right">
                            <a href="/positions" class="btn btn-secondary">Должности</a>
                        </div>
                    </div>
                    <br>
                </div>
            </div>

            @if(count($workers))

                <div class="row">
                    <div class="col-lg-12">
                        <table class="table">
                            <thead>
                            <tr>
                                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'name', 'label' => 'Имя'])
                                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'surname', 'label' => 'Фамилия'])
                                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'position', 'label' => 'Должность'])
                                @include('partials.dataTablesSortingTH', ['sort' => $sort, 'column' => 'salary', 'label' => 'Зарплата'])
                                <th scope="col" width="20%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($workers as $worker)
                                <tr>
                                    <td>{{ $worker->name }}</td>
                                    <td>{{ $worker->surname }}</td>
                                    <td>{{ $worker->position }}</td>
                                    <td>
                                        @if($worker->salary)
                                        {{ $worker->salary.' '.$worker->salary_currency }}
                                        @else
                                            Не указана
                                        @endif
                                    </td>
                                    <td>
                                        <div><a href="/workers/edit/{{ $worker->id }}" class="btn btn-primary">Редактировать</a>
                                        </div>
                                        <br>
                                        <div><a href="/workers/delete/{{ $worker->id }}"
                                                class="btn btn-danger">Удалить</a></div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div>
                            {!! $workers->appends(Request::all())->render() !!}
                        </div>
                    </div>
                </div>
            @else
                <div class="alert alert-primary" role="alert">Работники еще не были добавлены.</div>
            @endif


        </div>
    </div>

@endsection

@section('scripts')
    <script src="/template/js/jquery.sortItems.js"></script>
@endsection