<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Workers;
use App\Positions;
use App\Http\Requests\WorkersRequest;

class WorkersController extends Controller
{
	public function getIndex(Request $request) {

		$title = 'Работники';

		$workers = Workers::leftJoin('positions', 'workers.position_id', '=', 'positions.id')
			->select('workers.id', 'workers.name', 'workers.surname', 'workers.salary', 'workers.salary_currency', 'positions.name AS position');

		// Sorting
		$sort = $request->sort;
		if(count($sort)) {
			foreach ($sort as $column => $order) {
				if($order != null) {
					$flights = $workers->orderBy($column, $order);
				}
			}
		}
		else {
			$sort = [];
		}
		$nextOrder = ['0' => 'asc', 'asc' => 'desc', 'desc' => ''];

		$workers = $workers->paginate();

		return view('showWorkers', compact(['title', 'workers', 'sort', 'nextOrder']));
	}

	public function getAdd() {

		$title = 'Добавление работника';
		$worker = new Workers();

		$positions = self::_getPositionsArray();

		return view('editWorker', compact(['title', 'worker', 'positions']));
	}

	public function postAdd(WorkersRequest $request) {

		$worker = $request->except('_token');

		Workers::create($worker);

		$request->session()->flash('success-title', 'Добавлено');
		$request->session()->flash('success', 'Работник успешно добавлен!');

		$redirect = \Request::server('SERVER_NAME').'/workers';

		return ['redirect' => $redirect];
	}

	public function getEdit($id) {

		$title = 'Редактирование работника';

		$worker = Workers::findOrFail($id);

		$positions = self::_getPositionsArray();

		return view('editWorker', compact(['title', 'worker', 'positions']));
	}

	public function postEdit(WorkersRequest $request, $id) {

		$worker = $request->except('_token');

		Workers::findOrFail($id)->update($worker);

		$request->session()->flash('success-title', 'Обновлено');
		$request->session()->flash('success', 'Информация о работнике успешно обновлена!');

		$redirect = \Request::server('SERVER_NAME').'/workers';

		return ['redirect' => $redirect];
	}

	public function getDelete($id) {

		Workers::findOrFail($id)->delete();

		return redirect()->back()->with('success', 'Работник удален')->with('success-title', 'Удалено');
	}

	private static function _getPositionsArray() {

		$positionsData = Positions::select('name', 'id')->get();
		$positions = [];
		foreach($positionsData as $position) {
			$positions[$position->id] = $position->name;
		}

		return $positions;
	}
}
