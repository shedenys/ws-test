<?php

namespace App\Http\Controllers;

use App\Positions;
use App\Http\Requests\PositionsRequest;

class PositionsController extends Controller
{
    public function getIndex() {

    	$title = 'Должности';
    	$positions = Positions::paginate();

    	return view('showPositions', compact(['title', 'positions']));
    }

    public function getAdd() {

    	$title = 'Добавление должности';
    	$position = new Positions();

    	return view('editPosition', compact(['title', 'position']));
    }

    public function postAdd(PositionsRequest $request) {

	    $position = $request->except('_token');

	    Positions::create($position);

	    $request->session()->flash('success-title', 'Добавлено');
	    $request->session()->flash('success', 'Позиция успешно добавлена!');

	    $redirect = \Request::server('SERVER_NAME').'/positions';

	    return ['redirect' => $redirect];
    }

    public function getEdit($id) {

	    $title = 'Редактирование должности';
	    $position = Positions::findOrFail($id);

	    return view('editPosition', compact(['title', 'position']));
    }

	public function postEdit(PositionsRequest $request, $id) {

		$position = $request->except('_token');

		Positions::findOrFail($id)->update($position);

		$request->session()->flash('success-title', 'Обновлено');
		$request->session()->flash('success', 'Позиция успешно обновлена!');

		$redirect = \Request::server('SERVER_NAME').'/positions';

		return ['redirect' => $redirect];

	}

	public function getDelete($id) {

		Positions::findOrFail($id)->delete();

		return redirect()->back()->with('success', 'Должность удалена')->with('success-title', 'Удалено');
	}
}
